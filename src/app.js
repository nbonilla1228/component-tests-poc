const express = require('express');
const logger = require('./logger').log;
const app = express();
const port = require('./config').port;
const swaggerUi = require('swagger-ui-express');
const swaggerDocument = require('./api/swagger/swagger.json');
const healthController = require('./api/controllers/health');
const usersController = require('./api/controllers/user');






app.get('/api/testservice/v1/health', healthController.getHealth)
app.get('/api/testservice/v1/users', usersController.getUsers)
app.get('/api/testservice/v1/users/:id', usersController.getUser)

app.use('/api/testservice/v1', swaggerUi.serve, swaggerUi.setup(swaggerDocument));

app.listen(port);
logger.info(`Service listening on port ${port}`);