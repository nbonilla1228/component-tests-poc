const { createLogger, format, transports } = require('winston');
const { json } = format;

const log = createLogger({
    level: 'info',
    defaultMeta: { service: 'test-service'},
    transports: [
        new transports.Console({
            format: json(),
            level: 'info' }),
    ]
});

module.exports = {
    log
};