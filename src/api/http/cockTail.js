const cockTailUrl = require('../../config').cockTailURL
const logger = require('../../logger').log
const http = require('./http')

async function randomDrink() {
    logger.info(`Fetching Drink`)
    const body = await http.httpCall(`${cockTailUrl}/api/json/v1/1/random.php`);
    const drink = body.drinks[0].strDrink
    logger.info(`Drink returned is ${drink}`)
    return drink
}
module.exports = {
    randomDrink
}