const got = require('got');
const logger = require('../../logger').log

async function httpCall (url) {
    try {
        const body = await got(url, {responseType: 'json', resolveBodyOnly: true});
        return body
    }
    catch(e){
        logger.error(e);
        return e
    }

}

module.exports = {
    httpCall
}