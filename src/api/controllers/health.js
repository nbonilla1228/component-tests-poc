async function getHealth(req, res) {
    res.status(200).json({status:'Up'});
};

module.exports = {
    getHealth
};