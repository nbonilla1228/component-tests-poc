const logger = require('../../logger').log
const getDrink = require('../http/cockTail')

const users = [{
  userId: 1,
  name: 'Jill'
},
{
  userId: 2,
  name: 'Nate'
}]

async function getUsers(req, res) {
  const tempUsers = users;
  const drink = await getDrink.randomDrink()
  tempUsers[0]["favoriteDrink"] = drink;
  tempUsers[1]["favoriteDrink"] = drink;
  logger.info(`Returning all User`)
  res.status(200).json(tempUsers);
};

async function getUser(req, res) {
  const userId = req.params.id
  if (isNaN(userId)) {
    const errorMessage = `UserId - ${userId} is not a valid id, please try again with a number`
    logger.error(errorMessage)
    return res.status(400).json({ Message: errorMessage });
  }
  if (userId < 1 || userId > 2) {
    const message = `User Not Found`
    logger.info(message)
    return res.status(404).json({ Message: message })
  }
  else {
    const id = parseInt(userId);
    const tempUser = users[id - 1];
    const drink = await getDrink.randomDrink()
    tempUser["favoriteDrink"] = drink;
    return res.status(200).json(tempUser);
  }
};


module.exports = {
  getUser,
  getUsers
};