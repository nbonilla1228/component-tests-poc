module.exports = {
    cockTailURL: process.env.COCKTAILURL || 'https://www.thecocktaildb.com',
    port: process.env.PORT || 3500
}