Feature: GET Users

Scenario: Users
    When I call GET 'users'
    Then the status code should be 200
    And the response is equal to the file 'users.json'

Scenario: User 1
    When I call GET 'users/1'
    Then the status code should be 200
    # And the response is equal to the file 'userId1.json'

Scenario: User 2
    When I call GET 'users/2'
    Then the status code should be 200
    # And the response is equal to the file 'userId2.json'

Scenario: User Id doesn't exist
    When I call GET 'users/3'
    Then the status code should be 404
    Then the response should be: {"Message":"User Not Found"}

Scenario: User Id invalid
    When I call GET 'users/blue'
    Then the status code should be 400
    Then the response should be: {"Message":"UserId - blue is not a valid id, please try again with a number"}