Feature: GET Health

Scenario: Health Check
    When I call GET 'health'
    Then the status code should be 200
    And the response should be: {"status":"Up"}