const got = require('got');

async function Get(url) {
    try {
        const response = await got.get(`http://localhost:3500/api/testservice/v1/${url}`)
        return response;
    }
    catch (err) {
        return err.response;
    }
}

module.exports = {
    Get
}