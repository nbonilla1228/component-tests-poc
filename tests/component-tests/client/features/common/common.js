'use strict';

const httpService = require("./httpService");
const { Then, When } = require("@cucumber/cucumber");
const deepEqualInAnyOrder = require('deep-equal-in-any-order');
const chai = require('chai');
chai.use(deepEqualInAnyOrder);

const { expect } = chai;
const fs = require('fs');

When('I call GET {string}', async function (url) {
    this.response = await httpService.Get(url);
});

Then('the status code should be {int}', function (statusCode) {
    expect(this.response.statusCode).to.equal(statusCode);
});

Then('the response is equal to the file {string}', function (fileName) {
    const path = `./tests/component-tests/client/features/test-data/${fileName}`
    const buffer = fs.readFileSync(path);
    const expectedResponse = JSON.parse(buffer);
    const actualResponse = JSON.parse(this.response.body);
    expect(expectedResponse).to.deep.equalInAnyOrder(actualResponse);
});

Then(/^the response should be: (.*)$/, function(response) {
    expect(this.response.body).to.equal(response);
});