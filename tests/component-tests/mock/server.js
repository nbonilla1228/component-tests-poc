const { mock } = require('pactum');

mock.addInteraction({
  request: {
    method: 'GET',
    path: '/api/json/v1/1/random.php'
  },
  response: {
    status: 200,
    body: '{"drinks":[{"idDrink":"15224","strDrink":"Malibu Twister","strDrinkAlternate":null,"strTags":null,"strVideo":null,"strCategory":"Cocktail","strIBA":null,"strAlcoholic":"Alcoholic","strGlass":"Highball glass","strInstructions":"Add rum & trister then, add cranberry jucie,stir","strInstructionsES":null,"strInstructionsDE":"Rum und Trister hinzuf\u00fcgen, Preiselbeersaft zugeben, umr\u00fchren.","strInstructionsFR":null,"strInstructionsIT":"Aggiungere rum e la bevanda tropicana quindi, aggiungere il succo di mirtillo rosso, mescolare","strInstructionsZH-HANS":null,"strInstructionsZH-HANT":null,"strDrinkThumb":"https:\/\/www.thecocktaildb.com\/images\/media\/drink\/2dwae41504885321.jpg","strIngredient1":"Malibu rum","strIngredient2":"Tropicana","strIngredient3":"Cranberry juice","strIngredient4":null,"strIngredient5":null,"strIngredient6":null,"strIngredient7":null,"strIngredient8":null,"strIngredient9":null,"strIngredient10":null,"strIngredient11":null,"strIngredient12":null,"strIngredient13":null,"strIngredient14":null,"strIngredient15":null,"strMeasure1":"2 parts ","strMeasure2":"2 parts ","strMeasure3":"1 part ","strMeasure4":null,"strMeasure5":null,"strMeasure6":null,"strMeasure7":null,"strMeasure8":null,"strMeasure9":null,"strMeasure10":null,"strMeasure11":null,"strMeasure12":null,"strMeasure13":null,"strMeasure14":null,"strMeasure15":null,"strImageSource":null,"strImageAttribution":null,"strCreativeCommonsConfirmed":"No","dateModified":"2017-09-08 16:42:01"}]}'
  }
});

mock.start(3000);