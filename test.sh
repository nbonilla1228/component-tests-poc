set -e 
echo "Hi"
 TESTSERVICE=test_service

function cleanup {
    echo "Cleaning containers"
    array=($TESTSERVICE)
    for i in "$array"
    do
        id=$(docker ps -aqf "name=$i")
        if [ $(docker ps -aqf "name=$i") ]
        then
            echo "cleaning $i"
            docker stop $i
            docker rm $i
        fi
    done
}
cleanup
docker build -f tests/component-tests/service/Dockerfile -t $TESTSERVICE .

docker run --name $TESTSERVICE -p 4500:4500 $TESTSERVICE